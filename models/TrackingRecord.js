const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  identifier: { type: String, required: true },
}, { timestamps: true });

module.exports = mongoose.model('TrackingRecord', schema);