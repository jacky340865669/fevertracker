require('dotenv').config({ path: '/home/ec2-user/fevertracker/.env' });

const moment = require('moment');
const puppeteer = require('puppeteer');
const nodemailer = require('nodemailer');
const fs = require('fs');
const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    type: 'OAuth2',
    user: 'jacky340865669@gmail.com',
    clientId: process.env.GAPPS_ID,
    clientSecret: process.env.GAPPS_SECRET,
    refreshToken: process.env.GAPPS_REFRESH_TOKEN
  }
});

(async () => {

  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  let target = 'sony 50 18';
  let minPrice = '800';
  let maxPrice = '1500';
  let lastExecutedAt = getLastExecutedAt();
  let executedAt = moment();

  async function notifyEntries(entries){
    let html = entries
      .map(entry => `<a href="${entry.url}">${entry.title}</a> (${entry.price}, updated ${entry.updatedAt})`)
      .join('<br>');

    let message = {
      from: '"Fever Tracker" jacky340865669@gmail.com',
      to: 'jacky340865669@gmail.com',
      subject: `New Entries Found for ${target}`,
      html: html,
    };

    transporter.sendMail(message);
  }

  async function searchEntries(){
    await page.setViewport({
      width: 1024,
      height: 768
    });
    await page.goto('https://www.dcfever.com/trading/search.php');

    console.log('Entered DCFever search menu');

    await page.type('input[name=keyword]', target);


    await Promise.all([
      page.waitForNavigation(),
      page.click('form[name=search] input[type=submit]')
    ]);

    await page.type('div.refine_search > input[name=min_price]', minPrice);
    await page.type('div.refine_search > input[name=max_price]', maxPrice);

    await Promise.all([
      page.waitForNavigation(),
      page.click('#refine_submit')
    ]);

    console.log('Searching entries...');

    // parse DOM entries into entry objects
    let entries = await page.$$eval('table.trade_listing tr', rows => {
      // this function is evaluated in browser context
      return rows
        .map(row => {
          if(!row.querySelector('td:nth-child(6)')){ return null } // header and ad row does not have date column

          let title = row.querySelector('td:nth-child(3)').textContent;
          let url = row.querySelector('td:nth-child(3)').querySelector('a').href;
          let price = row.querySelector('td:nth-child(4)').textContent;
          let updatedAt = row.querySelector('td:nth-child(6)').textContent;

          return {
            title: title,
            url: url,
            price: price,
            updatedAt: updatedAt
          };
        })
        .filter(r => r); // remove empty rows
    });

    return entries;
  }

  function updateLastExecutedAt(){
    fs.writeFileSync('states.json', JSON.stringify({lastExecutedAt: executedAt.unix()}));
  }

  function getLastExecutedAt(){
    try{
      return moment.unix(JSON.parse(fs.readFileSync('states.json')).lastExecutedAt);
    }catch(err){
      return moment().subtract(1, 'momth');
    }
  }

  try{
    let tradeEntries = await searchEntries();

    console.log(`${tradeEntries.length} entries found`);

    // filter entries by updated date
    let latestTradeEntries = tradeEntries.filter(entry => {
      updatedMoment = moment(entry.updatedAt, "D/M HH:mm").utcOffset(+8, true);
      return updatedMoment > lastExecutedAt;
    });

    console.log(`${latestTradeEntries.length} new entries found`);

    if (latestTradeEntries.length > 0){
      // notify user of new entries
      notifyEntries(latestTradeEntries);
    }

    updateLastExecutedAt();
  }catch(e){
    console.log(e);
  };

  await browser.close();
})();