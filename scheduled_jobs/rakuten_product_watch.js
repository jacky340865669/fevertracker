const moment = require('moment');
const _ = require('lodash');

const scriptRunner = require('../modules/script_runner.js');
const { mapAsync, rejectAsync } = require('../modules/asyncHelper.js');
const rakuten = require('../modules/rakuten.js');
const slack = require('../modules/slack.js');
const TrackingRecord = require('../models/TrackingRecord.js');

// check whether item is recently notified
// if item is already notified, touch the record (so we know the item is not republished)
const isItemNotified = async (identifier) => {
  return TrackingRecord.findOneAndUpdate({
    identifier,
    updatedAt: { '$gte': moment().subtract(15, 'minutes') }
  }, {
    updatedAt: new Date()
  });
}

const markAsNotified = async (identifier) => {
  return TrackingRecord.findOneAndUpdate({
    identifier
  }, {}, { upsert: true });
}

const isTooExpensive = (mask) => {
  const price = parseFloat(mask.price.replace(/[,円]/g, '') || 0);

  let qty = _.min(
    Array.from(mask.title.matchAll(/([0-9,]+)枚/g))
      .map(match => { return parseInt(match[1]) })
  );
  // if no entries matched, _.min returns inf
  if (qty == Infinity) { qty = 1 }

  const sets = parseInt(
    (mask.title.match(/([0-9]+)(箱|[個コ袋]?セット)/) || [])[1] ||
    0
  );

  const totalQty = sets > 0 ? (sets * qty) : qty;

  mask.unitPrice = price / totalQty;

  const result = mask.unitPrice > 15;

  if (result) {
    console.log(`mask ${mask.title} ${mask.price} is too expensive (${mask.unitPrice} each)`);
  }

  return result;
}

const getMasks = async () => {
  console.log('searching medicom/prolane/pfe masks...');
  return rakuten.getProducts('メディコム+プロレーン+pfe/402788', {
    nitem: 'neoシリーズ ケース ガウン 3枚 5枚 7枚 晝夜 洗える ポスト投函対応商品',
    st: 'O',
  });
}

scriptRunner.start('rakuten_mask_watch', async () => {
  const allMasks = await getMasks();

  const matchedMasks = await rejectAsync(allMasks, async (mask) => {
    if(mask.isPreorder || mask.url.includes('careplus-fiore')) {
      console.log('skipping pre-order masks');
      return true;
    }

    if(isTooExpensive(mask)){
      console.log('skipping too expensive masks');
      return true
    }

    if(await isItemNotified(mask.url)) {
      console.log('skipping already notified item');
      return true;
    }

    return false;
  });

  if (matchedMasks.length == 0) {
    console.log('No matching Masks found, quitting...');
    return;
  }

  await slack.sendMessage('mask', JSON.stringify(matchedMasks, null, 1));

  await mapAsync(matchedMasks, async (mask) => {
    await markAsNotified(mask.url);
  });

  return;
});
