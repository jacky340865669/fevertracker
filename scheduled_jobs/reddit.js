require('dotenv').config({ path: '/home/ec2-user/fevertracker/.env' });

const moment = require('moment');
const nodemailer = require('nodemailer');
const fs = require('fs');
const fetch = require('node-fetch');
const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    type: 'OAuth2',
    user: 'jacky340865669@gmail.com',
    clientId: process.env.GAPPS_ID,
    clientSecret: process.env.GAPPS_SECRET,
    refreshToken: process.env.GAPPS_REFRESH_TOKEN
  }
});
const syncStateFileName = 'reddit_states.json';

(async () => {

  let lastExecutedAt = getLastExecutedAt();
  let executedAt = moment();

  async function notifyEntries(entries){
    let html = entries
      .map(entry => `<a href="${entry.url}">${entry.title}</a> (updated ${entry.updatedAt})`)
      .join('<br>');

    let message = {
      from: '"SSD Tracker" jacky340865669@gmail.com',
      to: 'jacky340865669@gmail.com',
      subject: `New SSD Entries Found`,
      html: html,
    };

    transporter.sendMail(message);
  }

  async function searchEntries(){
    console.log('Searching entries...');

    //search by keyword
    searchResponse = await fetch('https://gateway.reddit.com/desktopapi/v1/subreddits/buildapcsales/search?q=ssd&sort=new&t=all&type=link&restrict_sr=1&allow_over18=1&include=identity');
    // search by tag
    // searchResponse = await fetch('https://gateway.reddit.com/desktopapi/v1/subreddits/buildapcsales/search?q=flair_name%3A%22SSD%22&sort=new&t=all&type=link&include_over_18=1&restrict_sr=1&allow_over18=&include=identity');

    searchResponse = await searchResponse.json();
    let entries = Object.values(searchResponse.posts).map( (post) => {
      return {
        title: post.title,
        url: post.permalink,
        updatedAt: moment(post.created)
      };

      return entry;
    });

    return entries;
  }

  function updateLastExecutedAt(){
    fs.writeFileSync(syncStateFileName, JSON.stringify({lastExecutedAt: executedAt.unix()}));
  }

  function getLastExecutedAt(){
    try{
      return moment.unix(JSON.parse(fs.readFileSync(syncStateFileName)).lastExecutedAt);
    }catch(err){
      console.log('executed states not found, using default value (1 month ago)');
      return moment().subtract(1, 'month');
    }
  }

  try{
    let tradeEntries = await searchEntries();

    console.log(`${tradeEntries.length} entries found`);

    // filter entries by updated date
    // TODO: filter by price /(\$[0-9]+\.?[0-9]*)/
    // TODO: filter by storage size
    let latestTradeEntries = tradeEntries.filter(entry => {
      return entry.updatedAt > lastExecutedAt;
    });

    console.log(`${latestTradeEntries.length} new entries found`);

    if (latestTradeEntries.length > 0){
      // notify user of new entries
      notifyEntries(latestTradeEntries);
    }

    updateLastExecutedAt();
  }catch(e){
    console.log(e);
  };
})();