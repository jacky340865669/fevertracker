const Promise = require("bluebird");

const scriptRunner = require('../modules/script_runner.js');
const dou4la = require('../modules/dou4la.js');
const slack = require('../modules/slack.js');
const TrackingRecord = require('../models/TrackingRecord.js');

const isItemNew = async (identifier) => {
  const rec = await TrackingRecord.findOne({ identifier });
  return !rec;
}

const markAsNotified = async (identifier) => {
  return TrackingRecord.findOneAndUpdate({
    identifier
  }, {}, { upsert: true });
}


scriptRunner.start('dou4la_watch', async () => {
  const response = await dou4la.getReceivedParcels('OSAKA');

  const newPackages = await Promise.filter(
    response.aaData,
    pkg => { return isItemNew(pkg.ParcelId.toString()) },
  );

  if (newPackages.length === 0) {
    console.log('no new packages');
    return;
  }

  const description = newPackages.map( pkg => { return pkg.Description } ).join(', ');
  await slack.sendMessage(
    'mask',
    `${newPackages.length} DOU4LA package(s) have arrived HK. (${description})`
  );

  await Promise.map(
    newPackages,
    pkg => { return markAsNotified(pkg.ParcelId.toString()) },
  );

  return;
});
