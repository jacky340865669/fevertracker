const scriptRunner = require('../modules/script_runner.js');
const mailer = require('../modules/mailer.js');
const futbin = require('../modules/futbin.js');

scriptRunner.start('FUT Market Daily Summary', async () => {
  let score = await futbin.getMarketIndex(84);
  let subject = `[DAILY] Market84: ${score}`;
  console.log(subject);
  await mailer.sendMail({ subject });
});