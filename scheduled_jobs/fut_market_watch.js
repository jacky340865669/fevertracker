const scriptRunner = require('../modules/script_runner.js');
const mailer = require('../modules/mailer.js');
const futbin = require('../modules/futbin.js');
const DROP_TRIGGER = 120;
const RISE_TRIGGER = 210;

scriptRunner.start('FUT Market Watch', async () => {
  let score = await futbin.getMarketIndex(84);
  let subject = null;

  if (parseFloat(score) <= DROP_TRIGGER) {
    subject = `[DROP TRIGGERED] Market84: ${score} below ${DROP_TRIGGER}`;
  } else if (parseFloat(score) >= RISE_TRIGGER) {
    subject = `[RISE TRIGGERED] Market84: ${score} above ${RISE_TRIGGER}`;
  } else {
    console.log(`score ${score} does not meet trigger. `)
    return;
  }

  console.log(subject);
  await mailer.sendMail({ subject });
});
