const scriptRunner = require('../modules/script_runner.js');
const mailer = require('../modules/mailer.js');
const amazon = require('../modules/amazon.js');

scriptRunner.start('Amazon mask watch', async () => {
  const masks = await amazon.getProducts(
    'i=hpc&bbn=169922011&rh=n%3A160384011%2Cn%3A161669011%2Cn%3A169911011%2Cn%3A169922011%2Cp_n_feature_two_browse-bin%3A2054640051%2Cp_n_feature_browse-bin%3A2054634051%2Cp_n_price_fma%3A401041011%2Cp_72%3A82412051&dc&qid=1580232471&rnid=82409051&ref=sr_nr_p_72_1'
  );

  console.log(masks);

  const matchedMasks = masks.filter((product) => { return product.qty >= 30 });

  if (matchedMasks.length == 0) {
    console.log('No matching Masks found, quitting...');
    return;
  }

  await mailer.sendMail({
    subject: 'Amazon Masks found!',
    text: JSON.stringify(matchedMasks, null, 1),
  });

  return;
});
