const scriptRunner = require('../modules/script_runner.js');
const mailer = require('../modules/mailer.js');
const futbin = require('../modules/futbin.js');
const DROP_TRIGGER = 65000;

scriptRunner.start('FUT Player Watch', async () => {
  let price = await futbin.getPlayerPrice(294);

  if(parseFloat(price) > DROP_TRIGGER){
    console.log(`Drop trigger not met. ${price} is above trigger(${DROP_TRIGGER})`)
    return;
  };

  let subject = `[DROP TRIGGERED] Bernard Halloween: ${price} below ${DROP_TRIGGER}`;
  console.log(subject);
  await mailer.sendMail({ subject });
});
