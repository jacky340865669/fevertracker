const browser = require('../modules/browser.js')

async function getProducts(searchString) {
  return browser.simpleBrowser(async (page) => {
    await page.goto(`https://www.amazon.co.jp/s?${searchString}`);

    return page.evaluate(() => {
      const docs = [...document.querySelectorAll('.s-search-results .sg-col-inner')];

      return docs.map((doc) => {
        const title = doc.querySelector('h2 a span').textContent;
        const qty = parseInt((title.match(/([0-9])+枚/i) || [])[1]);
        return {
          title,
          qty,
          url: doc.querySelector('h2 a').href
        }
      });
    });
  })
}

module.exports = {
  getProducts
}