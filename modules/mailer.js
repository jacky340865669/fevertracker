const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    type: 'OAuth2',
    user: 'jacky340865669@gmail.com',
    clientId: process.env.GAPPS_ID,
    clientSecret: process.env.GAPPS_SECRET,
    refreshToken: process.env.GAPPS_REFRESH_TOKEN
  }
});

// allowed params: title, text, etc
// doc: https://nodemailer.com/message/
async function sendMail(params){

  let message = {
    from: '"JTracker" jacky340865669@gmail.com',
    to: 'jacky340865669@gmail.com',
    ...params
  };

  transporter.sendMail(message);
}

module.exports = {
  sendMail
}


