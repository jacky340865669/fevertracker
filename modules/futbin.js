const browser = require('../modules/browser.js')

async function getMarketIndex(rating){
  let textContent = await browser.simpleBrowser(async (page) => {
    await page.goto(`https://www.futbin.com/market/${rating}`);

    const textContent = await page.evaluate(() => {
      return document.querySelector('.market_index_th').textContent;
    });

    return textContent;
  })

  const [score, delta] = textContent.split(/\s+/).filter(Boolean);

  return score;
}

async function getPlayerPrice(playerId){
  let textContent = await browser.simpleBrowser(async (page) => {
    await page.goto(`https://www.futbin.com/20/player/${playerId}`);

    await page.waitForFunction(() => {
      document.querySelector('#ps-lowest-1').textContent !== '-'
    });

    const textContent = await page.evaluate(() => {
      return document.querySelector('#ps-lowest-1').textContent;
    });

    return textContent;
  })

  return parseInt(textContent.replace(',',''));
}

module.exports = {
  getMarketIndex,
  getPlayerPrice
}