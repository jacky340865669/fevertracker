const mapAsync = (array, callbackfn) => {
  return Promise.all(array.map(callbackfn));
}

const eachAsync = mapAsync;

const filterAsync = async (array, callbackfn) => {
  const filterMap = await mapAsync(array, callbackfn);
  return array.filter((value, index) => filterMap[index]);
}

const rejectAsync = async (array, callbackfn) => {
  const filterMap = await mapAsync(array, callbackfn);
  return array.filter((value, index) => !filterMap[index]);
}

const timeoutAsync = async(ms) => {
  return new Promise(resolve => setTimeout(resolve, ms));
}

module.exports = { mapAsync, eachAsync, filterAsync, rejectAsync, timeoutAsync }