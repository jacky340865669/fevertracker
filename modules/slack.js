const { IncomingWebhook } = require('@slack/webhook');

// ref: https://slack.dev/node-slack-sdk/webhook
async function sendMessage(webhook_type, params){
  const url = process.env[`SLACK_WEBHOOK_${webhook_type.toUpperCase()}_URL`]
  const webhook = new IncomingWebhook(url);

  return webhook.send(params);
}

module.exports = {
  sendMessage
}


