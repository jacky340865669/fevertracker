const browser = require('../modules/browser.js')
const querystring = require('querystring');
const { timeoutAsync } = require('../modules/asyncHelper.js');

async function getReceivedParcels(warehouse) {
  return browser.simpleBrowser(async (page) => {
    await page.goto(`https://dou4la.com/Account/Login`);

    await page.type('#EmailOrPhone', process.env.DOU4LA_EMAIL, {delay: 50});
    await page.type('#Password', process.env.DOU4LA_PASSWORD, {delay: 50});
    await Promise.all([
      page.waitForNavigation(),
      page.click('#Signin', {delay: 500}),
    ]);

    await page.click('#main-wrapper > aside > div.sidebar-country > div.sidebar-country-menu > div > a')

    const warehouseSelector = `#current_country > li:nth-child(2) > a[data-value=${warehouse}]`;

    await page.waitForSelector(warehouseSelector);

    await timeoutAsync(3000);

    await Promise.all([
      page.waitForNavigation(),
      page.click(warehouseSelector, {delay: 500}),
    ]);

    return page.evaluate(async () => {
      const res = await fetch(
        "https://dou4la.com/Parcel/LoadReceivedParcelAjax", {
          "credentials":"include",
          "headers":{
            "accept":"application/json, text/javascript, */*; q=0.01",
            "accept-language":"zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-CN;q=0.6,ja;q=0.5",
            "cache-control":"no-cache",
            "pragma":"no-cache",
            "sec-fetch-mode":"cors",
            "sec-fetch-site":"same-origin",
            "x-requested-with":"XMLHttpRequest"
          },
          "referrer":"https://dou4la.com/Parcel/ReceivedParcel",
          "referrerPolicy":"no-referrer-when-downgrade",
          "body":null,
          "method":"GET",
          "mode":"cors"
        }
      );

      return res.json();
    })
  })
}

module.exports = {
  getReceivedParcels
}