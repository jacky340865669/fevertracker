require('dotenv').config({ path: `${process.env.PWD}/.env` });

const mailer = require('../modules/mailer.js');
const mongoose = require('mongoose');
const axios = require('axios');

async function start(scriptName, mainFunc) {
  try {
    console.log(`=== Script ${scriptName} begin at ${new Date()} ===`);

    await mongoose.connect(process.env.MONGO_CONNECTION_URL, {
      useNewUrlParser: true,
      autoReconnect: true,
    });

    await mainFunc();
    console.log(`=== Script ${scriptName} finished successfully at ${new Date()} ===`);

    const healthCheckUrl = process.env[`HEALTH_CHECK_${scriptName.toUpperCase()}_URL`];

    if (healthCheckUrl) {
      await axios.get(healthCheckUrl);
    }

    process.exit(0);
  }catch(e){
    //FIXME: sometimes the this catch phase is bypassed

    const details = {
      error: e.toString(),
      stack: e.stack,
      failedAt: new Date()
    };
    console.log(e);
    console.log(`=== Script ${scriptName} finished with exception at ${details.failedAt} ===`);

    await mailer.sendMail({
      subject: `[JOB FAILED] ${scriptName} crashed`,
      text: JSON.stringify(details, null, 1),
    });

    process.exit(1);
  }
};

module.exports = {
  start
}