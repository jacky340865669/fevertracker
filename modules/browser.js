const puppeteer = require('puppeteer');

async function simpleBrowser(func){
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  await page.setViewport({
    width: 1024,
    height: 768
  });

  let result = await func(page);

  await browser.close();

  return result;
}

module.exports = {
  puppeteer,
  simpleBrowser
}
