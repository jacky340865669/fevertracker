const browser = require('../modules/browser.js')
const querystring = require('querystring');

async function getProducts(keyword, customQueryParams) {
  return browser.simpleBrowser(async (page) => {
    const queryParams = {
      max: (10000 + Math.random() * 9999),
      v: 2,
      ...customQueryParams,
    }
    await page.goto(`https://search.rakuten.co.jp/search/mall/${keyword}/?${querystring.stringify(queryParams)}`);

    return page.evaluate(() => {
      const docs = [...document.querySelectorAll(".searchresultitem")];

      return docs.map((doc) => {
        return {
          isPreorder: ((doc.querySelector('.dui-tag') || {}).textContent == '販売期間前'),
          title: (doc.querySelector('.title h2 a') || {}).textContent,
          shippingDate: ((doc.querySelector('.shipping') || {}).textContent || '').replace(/\s/g, ""),
          price: (doc.querySelector('.important') || {}).textContent,
          url: (doc.querySelector('.title h2 a') || {}).href
        }
      });
    });
  })
}

module.exports = {
  getProducts
}